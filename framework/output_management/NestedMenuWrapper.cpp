#ifndef NESTED_MENU_WRAPPER_CPP
#define NESTED_MENU_WRAPPER_CPP

#include <signal.h>
#include <unistd.h>
#include "NestedMenuWrapper.h"

NestedMenuWrapper::NestedMenuWrapper(NestedMenu *menu, WINDOW *menuWindow) {
    this->currentMenu = menu;
    this->menuWindow = menuWindow;
    this->drawWindow();
}

void NestedMenuWrapper::drawWindow() {
    wclear(this->menuWindow);

    this->currentMenu->recreateMenu();

    set_menu_win(this->currentMenu->menu, this->menuWindow);
    set_menu_sub(this->currentMenu->menu, derwin(this->menuWindow, 0, 0, 0, 0));
    post_menu(this->currentMenu->menu);

    // draw the windows
    refresh();
    wrefresh(this->menuWindow);
}

MENU *NestedMenuWrapper::getMenu() {
    return this->currentMenu->menu;
}

NestedMenuWrapper::~NestedMenuWrapper() {}

std::string NestedMenuWrapper::handleKey(int c) {
    switch (c) {
        // TODO: REFACTOR, pleeeeeease!
        case KEY_LEFT: {
            if (currentMenu->parentItem != nullptr) {
                this->currentMenu = currentMenu->parentItem->parentMenu;
                this->drawWindow();
            }
            auto selectedItemObject = currentMenu->items[item_index(current_item(this->currentMenu->menu))];
            if (selectedItemObject->subMenu == nullptr || selectedItemObject->subMenu->isEmpty()) {
                // the selected ItemObject has not a submenu
                return selectedItemObject->name;
            } else {
                return "NOTHING";
            }        }
        case KEY_DOWN: {
            menu_driver(this->currentMenu->menu, REQ_DOWN_ITEM);
            auto selectedItemObject = currentMenu->items[item_index(current_item(this->currentMenu->menu))];
            if (selectedItemObject->subMenu == nullptr || selectedItemObject->subMenu->isEmpty()) {
                // the selected ItemObject has not a submenu
                return selectedItemObject->name;
            } else {
                return "NOTHING";
            }
        }
        case KEY_UP: {
            menu_driver(this->currentMenu->menu, REQ_UP_ITEM);
            auto selectedItemObject = currentMenu->items[item_index(current_item(this->currentMenu->menu))];
            if (selectedItemObject->subMenu == nullptr || selectedItemObject->subMenu->isEmpty()) {
                // the selected ItemObject has not a submenu
                return selectedItemObject->name;
            } else {
                return "NOTHING";
            }
        }
        case KEY_RIGHT:
        case 0xA: {// Enter-Key
            auto selectedItemObject = currentMenu->items[item_index(current_item(this->currentMenu->menu))];
            if (selectedItemObject->subMenu == nullptr || selectedItemObject->subMenu->isEmpty()) {
                // the selected ItemObject has not a submenu
                return selectedItemObject->name;
            } else {
                // the selected ItemObject does have a submenu -> go to sub menu
                this->currentMenu = selectedItemObject->subMenu;
                this->drawWindow();

                selectedItemObject = currentMenu->items[item_index(current_item(this->currentMenu->menu))];
                if (selectedItemObject->subMenu == nullptr || selectedItemObject->subMenu->isEmpty()) {
                    // the selected ItemObject has not a submenu
                    return selectedItemObject->name;
                } else {
                    return "NOTHING";
                }
            }
        }
        default:
            return "NOTHING";
    }
}

std::vector<std::string> NestedMenuWrapper::getSelectedMenuPath() {
    auto menuPath = std::deque<std::string>();
    auto menuIterator = this->currentMenu;
    while (menuIterator != nullptr) {
        auto itemName = menuIterator->items[item_index(current_item(menuIterator->menu))]->name;
        menuPath.push_front(itemName);
        if(menuIterator->parentItem)
            menuIterator = menuIterator->parentItem->parentMenu;
        else 
            break;
    }
    return {menuPath.begin(), menuPath.end()};
}

#endif
