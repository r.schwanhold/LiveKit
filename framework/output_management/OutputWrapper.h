#ifndef LIVEKIT_OUTPUTWRAPPER_H
#define LIVEKIT_OUTPUTWRAPPER_H

#include <string>
#include <sstream>

#include "OutputManager.h"

class OutputManager;

class OutputWrapper {
    std::vector<std::string> menuPath;

    OutputManager *outputManager;

    std::ostream *stream;

    friend class OutputManager;
public:
    OutputWrapper();

    OutputWrapper(std::vector<std::string> menuPath, OutputManager *outputManager, std::ostream* stream);

    template<typename T>
    OutputWrapper &operator<<(T input) {
        (*stream) << input;
        if (this->outputManager)
            outputManager->notify(this->menuPath);
        return *this;
    }

    OutputWrapper &operator<<( std::ostream&(*endl)(std::ostream&) );

    void setCycle(uint16_t cycle);
};



#endif //LIVEKIT_OUTPUTWRAPPER_H
