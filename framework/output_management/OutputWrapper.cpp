#include "OutputWrapper.h"

OutputWrapper::OutputWrapper() : menuPath{}, outputManager(nullptr), stream(nullptr) {}

OutputWrapper::OutputWrapper(std::vector<std::string> menuPath, OutputManager *outputManager, std::ostream* stream)
: menuPath(menuPath)
, outputManager(outputManager)
, stream(stream) {}

OutputWrapper &OutputWrapper::operator<<( std::ostream&(*endl)(std::ostream&) ) {
    (*stream) << endl;
    return *this;
};

void OutputWrapper::setCycle(uint16_t cycle) {
    if (this->outputManager)
        this->outputManager->updateCycle(this->menuPath, cycle);
}