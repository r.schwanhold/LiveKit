#ifndef ITEM_OBJECT_CPP
#define ITEM_OBJECT_CPP

#include "ItemObject.h"

ItemObject::ItemObject(std::string name) {
    this->name = name;
    this->item = new_item(this->name.c_str(), "");
    this->subMenu = nullptr;
    this->parentMenu = nullptr;
}

ItemObject::ItemObject(std::string name, NestedMenu *subMenu) {
    this->name = name;
    this->item = new_item(this->name.c_str(), "");
    this->subMenu = subMenu;
    this->subMenu->parentItem = this;
    this->parentMenu = nullptr;
}

ItemObject::~ItemObject() {
    free_item(this->item);
    delete this->subMenu;
}

#endif
