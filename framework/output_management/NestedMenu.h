#ifndef NESTED_MENU_H
#define NESTED_MENU_H

#include <vector>
#include <menu.h>
#include <iostream>
#include <signal.h>
#include <cstring>
#include "ItemObject.h"

class ItemObject;

class NestedMenu {
private:
    bool initialized = false;

    std::vector<ITEM *> pureItems;

public:
    std::vector<ItemObject *> items;
    ItemObject *parentItem;
    MENU *menu;

    void recreateMenu();

    NestedMenu();

    void addItem(ItemObject *item);

    /**
     * Initializes the menu with all Items that have been added so far.
     * Adding Items later is not guaranteed to work.
     */
    void initializeMenu();

    bool isEmpty();

    ~NestedMenu();
};

#endif