#ifndef OUTPUT_INTERFACE_H
#define OUTPUT_INTERFACE_H

#include <stdlib.h>

class OutputInterface {
protected:
    OutputInterface() = default;

public:

    virtual void quit() = 0;

    virtual void handle_winch(int sig) = 0;

    virtual void start() = 0;

    virtual void setCycleCount(int cycleCount){
        (void) cycleCount; // UNUSED
    };

    virtual void updateProgress(float newProgress) = 0;

    virtual void updateCycle(std::vector<std::string> menuPath, int newCycle) = 0;

    virtual void notify(std::vector<std::string> menuPath) = 0;

    virtual std::ostream *registerOStream(std::vector<std::string> menuPath) = 0;

    virtual void captureStream(std::vector<std::string> menuPath, std::ostream &stream) = 0;
};

#endif