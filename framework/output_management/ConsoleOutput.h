#ifndef TUI_MENU_NULL_H
#define TUI_MENU_NULL_H

#include <stdlib.h>
#include <string>
#include <sstream>
#include "OutputInterface.h"

class ConsoleOutput : public OutputInterface {
private:
    std::stringstream *ss;
protected:
    ConsoleOutput() : ss(new std::stringstream){
    };

public:
    void quit() override {};

    void handle_winch(int sig) override {
        (void) sig; // UNSUSED
    };

    void start() override {};

    void updateProgress(float newProgress) override {
        (void) newProgress; // UNSUSED
    };

    void updateCycle(std::vector<std::string> menuPath, int newCycle) override {
        (void) menuPath; // UNUSED
        (void) newCycle; // UNUSED
    };

    void notify(std::vector<std::string> menuPath) override {
        (void) menuPath; // UNUSED
    };

    std::ostream *registerOStream(std::vector<std::string> menuPath) override {
        (void) menuPath; // UNUSED
        return &cout;
    };

    void captureStream(std::vector<std::string> menuPath, std::ostream &stream) override {
        (void) menuPath; // UNUSED
        (void) stream; // UNUSED
    };

    static OutputInterface &getInstance() {
        static ConsoleOutput instance;
        return instance;
    }
};

#endif