#ifndef ITEM_OBJECT_H
#define ITEM_OBJECT_H

#include "NestedMenu.h"

class NestedMenu;

class ItemObject {
public:
    NestedMenu *parentMenu;
    NestedMenu *subMenu;
    ITEM *item;
    std::string name;

    ItemObject(std::string name);

    ItemObject(std::string name, NestedMenu *subMenu);

    ~ItemObject();
};

#endif
