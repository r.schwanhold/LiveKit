set(FRAGMENT_SOURCES
        ${CMAKE_CURRENT_SOURCE_DIR}/Fragment.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Fragment.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/FragmentContainer.h
        ${CMAKE_CURRENT_SOURCE_DIR}/FragmentContainer.cpp
        PARENT_SCOPE)
