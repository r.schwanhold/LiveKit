#ifndef LIVEKIT_SEQUENCEREPRESENTATIONMANAGER_H
#define LIVEKIT_SEQUENCEREPRESENTATIONMANAGER_H

#include <stdint.h>
#include "../data_representation/SequenceContainer.h"
#include <map>

/**
 * The `SequenceManager` bundles all `Sequences` objects to carry a uniform representation and exposes an interface to get a specific `Sequences` object.
 */
class SequenceManager {
private:
    std::map<uint16_t, std::map<uint16_t, SequenceContainer>> sequenceRepresentations;
#ifdef TEST_DEFINITIONS
    friend class SequenceManagerAccessHelper;
#endif

public:
    SequenceManager(const std::vector<uint16_t>& lanes, const std::vector<uint16_t>& tiles, std::vector<std::pair<int, char>> readstructure);

    SequenceContainer& getSequences(uint16_t lane, uint16_t tile);

    bool serializeSequencesOfTile(uint16_t lane, uint16_t tile);

    bool deserializeSequencesOfTile(uint16_t lane, uint16_t tile);

    bool areSequencesOfTileSerialized(uint16_t lane, uint16_t tile);
};

#endif //LIVEKIT_SEQUENCEREPRESENTATIONMANAGER_H
