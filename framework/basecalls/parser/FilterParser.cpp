#include <boost/filesystem.hpp>
#include "FilterParser.h"
#include "../../utils.h"

using namespace std;

FilterParser::FilterParser(vector<uint16_t> &lanes, vector<uint16_t> &tiles, FilterRepresentation *filterRepresentation)
    : lanes(lanes), tiles(tiles), filterRepresentation(filterRepresentation) {}

void FilterParser::parse(uint16_t lane, uint16_t tile, const string &path, int numReads) {
    string filterFileName = utils::getFilterFilename(lane, tile, path);
    if (boost::filesystem::exists(filterFileName)){
        std::vector<char> filterData;
        int position = 12; // the real data starts at index 12
        utils::readBinaryFile(filterFileName, filterData, position);
        this->filterRepresentation->addFilterData(lane, tile, filterData);
    } else {
        this->filterRepresentation->initializeDefault(lane, tile, numReads);
    }

}
