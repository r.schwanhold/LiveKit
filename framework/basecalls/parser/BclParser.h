#ifndef LIVEKIT_BCLPARSER_H
#define LIVEKIT_BCLPARSER_H

#include <sstream>
#include <fstream>
#include <iomanip>
#include <cstring>
#include "../data_representation/BclRepresentation.h"

class BclParser {
protected:
    BclRepresentation *bclRepresentation;

public:
    BclParser(BclRepresentation *bclRepresentation) :
            bclRepresentation(bclRepresentation) {}

    virtual void parse(uint16_t lane, uint16_t tile, uint16_t cycle, const std::string &bclPath);

    /**
     * Returns a vector of filenames.
     * The BclParser returns a vector with only one element (the path to the .bcl file)
     * @param lane
     * @param tile
     * @param cycle
     * @param path
     * @return
     */
    virtual std::vector<std::string> getFilenames(uint16_t lane, uint16_t tile, uint16_t cycle, const std::string &path);

    virtual ~BclParser() = default;
};

#endif //LIVEKIT_BCLPARSER_H
