#include "FileDetector.h"
#include "utils.h"

#ifdef __linux__
#include <sys/inotify.h>
#endif

#include <boost/filesystem.hpp>
#include <cstddef>
#include <iostream>

#define MAX_EVENTS 1024 /*Max. number of events to process at one go*/
#define LEN_NAME 16 /*Assuming that the length of the filename won't exceed 16 bytes*/
#define EVENT_SIZE  (sizeof(struct inotify_event) ) /*size of one event*/
#define BUF_LEN (MAX_EVENTS * (EVENT_SIZE + LEN_NAME)) /*buffer to store the data of events*/

using namespace std;

FileDetector::FileDetector(std::string path) : rootPath(path) {}

void FileDetector::waitForDirectory(std::string path, bool isFile) {
#ifdef __APPLE__

    while(!boost::filesystem::exists(path)) {
        usleep(100000); //100 ms
    }

    return;

#elif __linux__

    if (boost::filesystem::exists(path)) return;

    //Get root directory of the wanted directory
    std::size_t found = path.find_last_of('/');
    std::string rootPath = path.substr(0, found);

    //Get directory name without full path
    std::string fileName = path.substr(found + 1);

    //Set up inotify for root directory
    int fileDescriptor = inotify_init();

    if (fileDescriptor < 0) {
        std::cout << "Couldn't add watch to path" << std::endl;
        perror("Could not initialize inotify");
    }

    auto signal = isFile ? IN_CLOSE : IN_CREATE;
    int watchDescriptor = inotify_add_watch(fileDescriptor, rootPath.c_str(), signal);

    if (watchDescriptor == -1) {
        std::cout << "Couldn't add watch to path" << std::endl;
        perror("inotify_add_watch");
    }

    char buffer[BUF_LEN];
    struct inotify_event *event;

    if (boost::filesystem::exists(path)) {
        inotify_rm_watch(fileDescriptor, watchDescriptor);
        close(fileDescriptor);
        return;
    }

    std::cout << "Waiting for " << path << " ..." << std::endl;

    while (true) {
        int i = 0;

        //Execution stops here, until the searched directory has been created
        int length = read(fileDescriptor, buffer, BUF_LEN);

        while (i < length) {
            event = (struct inotify_event *) &buffer[i];

            if (event->len) {

                bool directoryFound =
                        (event->mask & signal) &&
                        (!strcmp(&(event->name)[0], fileName.c_str()));

                if (directoryFound) {
                    if(!isFile)
                        std::cout << "Directory " << std::string(event->name) << " found" << std::endl;

                    inotify_rm_watch(fileDescriptor, watchDescriptor);
                    close(fileDescriptor);

                    return;
                }

                i += sizeof(struct inotify_event) + event->len;
            }
        }
    }

#endif
}

void FileDetector::waitForFile(std::string filename){
    // wait for parent directory
    int lastOccurency = filename.find_last_of('/');
    string parentDirectoryName = filename.substr(0, lastOccurency);
    this->waitForDirectory(parentDirectoryName, false);

    // wait for file
    this->waitForDirectory(filename, true);
}

void FileDetector::waitForAllLanes(vector<uint16_t> lanes, std::string path, bool isFile) {
    for (auto lane : lanes)
        this->waitForDirectory(this->rootPath + "/L" + utils::toNDigits(lane, 3) + path, isFile);
}

void FileDetector::waitForCycleFiles(std::vector<std::string> filenames) {
    for(const auto& filename : filenames)
        this->waitForFile(filename);

}
