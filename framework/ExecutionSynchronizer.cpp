#include "ExecutionSynchronizer.h"

using namespace std;

ExecutionSynchronizer::ExecutionSynchronizer(int numThreads) : numThreads(numThreads) {}

void ExecutionSynchronizer::waitForStepAvailable() {
    unique_lock<mutex> lock(this->stepAvailableMutex);
    this->stepAvailableConditionVariable.wait(lock);
}

void ExecutionSynchronizer::notifyStepAvailable() {
    this->stepAvailableConditionVariable.notify_all();
}

void ExecutionSynchronizer::waitForCycleAvailable() {
    unique_lock<mutex> lock(this->cycleAvailableMutex);
    this->cycleAvailableConditionVariable.wait(lock);
}

void ExecutionSynchronizer::notifyCycleAvailable() {
    this->cycleAvailableConditionVariable.notify_all();
}
