#ifndef LIVEKIT_PYTHONPLUGIN_H
#define LIVEKIT_PYTHONPLUGIN_H


#include <boost/python.hpp>
#include "../Plugin.h"

namespace bp = boost::python;

LIVEKIT_PLUGIN(PythonPlugin) {

    /*
     * PythonPluginTileContext needs access to 'runPythonMethod'. In C++ inner classes do not know the outer classes by default.
     * To mitigate this, we need to give the 'PythonPluginTileContext' a reference to 'PythonPlugin'.
     * That is only possible if we provide the right constructor.
     * That has as a consequence that we no longer can use the preprocessor command.
     */

    TileContext *getTileContext() override {
        return new PythonPluginTileContext(innerClass);
    };

    class PythonPluginTileContext : public Plugin::TileContext {
        private:
            PythonPlugin& outerClass;
        public:
            explicit PythonPluginTileContext(PythonPlugin& outerClass): outerClass(outerClass) {}
            std::shared_ptr<FragmentContainer> runCycleForTile(std::shared_ptr<FragmentContainer> inputFragments) override;
    };


private:
    PythonPluginTileContext innerClass;

    bp::object pluginHandle;

    bp::object import(const std::string &path);

    std::string getFileNameFromPath(std::string path);

    std::shared_ptr<FragmentContainer> runPythonMethod(const char *name, std::shared_ptr<FragmentContainer> inputFragments, uint16_t lane, uint16_t tile);

public:
    PythonPlugin() : innerClass(*this) {}

    void setConfig() override;

    void init() override;

    std::shared_ptr<FragmentContainer> runPreprocessing(std::shared_ptr<FragmentContainer> inputFragments) override;

    std::shared_ptr<FragmentContainer> runCycle(std::shared_ptr<FragmentContainer> inputFragments) override;

    bool hasTileContext() override;

    std::shared_ptr<FragmentContainer> runFullReadPostprocessing(std::shared_ptr<FragmentContainer> inputFragments) override;

    void setCurrentPluginCycle(int cycle) override;

    void finalize() override;
};

#endif //LIVEKIT_PYTHONPLUGIN_H
