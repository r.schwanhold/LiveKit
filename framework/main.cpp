#include "main.h"
#include <boost/filesystem.hpp>
#include <iostream>

#include "Framework.h"

using namespace std;

int main(int argc, char **argv) {
    // Create a temp directory for all output files
    boost::filesystem::create_directory(boost::filesystem::path("./temp"));
    boost::filesystem::create_directories("./temp/sequences/");

    string frameworkManifestFilePath = (argc == 2) ?  argv[1] : "../config/framework.json";
    Framework framework(frameworkManifestFilePath);
    framework.run();

    // TODO: Delete temp folder

    return 0;
}
