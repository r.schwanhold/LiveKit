#ifndef LIVEKIT_FILEDETECTOR_H
#define LIVEKIT_FILEDETECTOR_H

#include <string>
#include <vector>

class FileDetector {
private:

    std::string rootPath;

public:

    explicit FileDetector(std::string path);

    void waitForDirectory(std::string path, bool isFile = false);

    void waitForFile(std::string filename);

    void waitForCycleFiles(std::vector<std::string> filenames);

    void waitForAllLanes(std::vector<uint16_t> lanes, std::string path = "", bool isFile = false);
};

#endif //LIVEKIT_FILEDETECTOR_H