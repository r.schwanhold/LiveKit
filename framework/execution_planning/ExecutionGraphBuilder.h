#ifndef LIVEKIT_EXECUTIONGRAPHBUILDER_H
#define LIVEKIT_EXECUTIONGRAPHBUILDER_H

#include <vector>
#include <string>
#include <map>
#include <deque>
#include <functional>

#include "ExecutionGraph.h"

class ExecutionGraphBuilder {

#ifdef TEST_DEFINITIONS

public:

#endif

    ExecutionGraph *graph;

    typedef ExecutionGraph::Vertex Vertex;
    typedef ExecutionGraph::Edge Edge;

    std::vector<uint16_t> lanes;

    std::vector<uint16_t> tiles;

    std::map<VertexIdentifier, Vertex> pluginVertices;

    struct OutputSpecificationKey;
    typedef struct OutputSpecificationKey OutputSpecificationKey;

    struct OutputSpecificationKey {
      std::string fragmentName;
      uint16_t lane;
      uint16_t tile;
      int cycle;

      // Add operator< to make the struct usable as the key of a map
      bool operator<(const OutputSpecificationKey &key) const {
          return fragmentName < key.fragmentName
              || (fragmentName == key.fragmentName && lane < key.lane)
              || (fragmentName == key.fragmentName && lane == key.lane && tile < key.tile)
              || (fragmentName == key.fragmentName && lane == key.lane && tile == key.tile && cycle < key.cycle);
      }
    };

    struct OutputSpecificationValue;
    typedef struct OutputSpecificationValue OutputSpecificationValue;

    struct OutputSpecificationValue {
      std::string fragmentType;
      Vertex vertex;

      bool operator==(const OutputSpecificationValue &val) const {
          return fragmentType == val.fragmentType && vertex == val.vertex;
      }
    };

    std::map<OutputSpecificationKey,OutputSpecificationValue> registeredOutputSpecifications;

    Vertex toVertex(VertexIdentifier identifier);

    TEST_VIRTUAL void registerOutputSpecification(
        PluginSpecification::FragmentSpecification outputFragmentSpecification,
        Vertex vertex,
        VertexIdentifier identifier);

    Vertex addVertex(VertexIdentifier identifier);

    TEST_VIRTUAL Edge addEdge(
        Vertex sourceVertex,
        Vertex targetVertex,
        PluginSpecification::FragmentSpecification fragmentSpecification = {"", "", false}
    );

    void addInputEdges(Vertex targetVertex);

    OutputSpecificationValue searchForOutputSpecification(
        PluginSpecification::FragmentSpecification inputSpecification,
        std::vector<OutputSpecificationKey> specificationKeys
    );

public:
    ExecutionGraphBuilder(std::vector<uint16_t> lanes, std::vector<uint16_t> tiles, int cycleCount);

    ExecutionGraph *build(std::set<Plugin *> plugins);

    void write_graphviz();

    TEST_VIRTUAL ~ExecutionGraphBuilder() = default;
};

#endif //LIVEKIT_EXECUTIONGRAPHBUILDER_H
