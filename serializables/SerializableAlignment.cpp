#include "SerializableAlignment.h"
#include "../plugins/HiLive_sharedLibraries/tools.h"
#include "../plugins/HiLive_sharedLibraries/tools_static.h"
#include "../plugins/HiLive_sharedLibraries/alnstream.h"

using namespace std;

SerializableAlignment::SerializableAlignment(char *serializedSpace, std::string &serializationDirectory) {
    this->deserialize(serializedSpace, serializationDirectory);
}

void SerializableAlignment::serializeData(char *serializedSpace, string &serializationDirectory) {
    cout << "--> Alignment " << this->lane << " " << this->tile << endl;

    SERIALIZE_VALUE(this->lane);
    SERIALIZE_VALUE(this->tile);
    SERIALIZE_VALUE(this->rlen);
    SERIALIZE_VALUE(this->mate);
    SERIALIZE_VALUE(this->readId);
    SERIALIZE_VALUE(this->numReads);
    SERIALIZE_VALUE(this->memorySaveMode);
    SERIALIZE_VALUE(this->currentReadCycle);
    SERIALIZE_VALUE(this->blockSize);
    SERIALIZE_VALUE(this->compressedFormat);

    if (!this->memorySaveMode) {
        this->create_directories(serializationDirectory);

        string outputFilename = get_align_fname(this->lane, this->tile, this->currentReadCycle, this->mate, serializationDirectory);
        outputFilename += ".fragment";
        this->writeAlignFile(outputFilename);
    }
}

void  SerializableAlignment::writeAlignFile(string &outputFilename) {

    oAlnStream output(
        this->lane,
        this->tile,
        this->currentReadCycle,
        this->rlen,
        this->numReads,
        this->blockSize,
        this->compressedFormat
    );

    output.open(outputFilename);

    for (auto alignment : this->alignments) output.write_alignment(alignment);

    output.close();
}

void SerializableAlignment::deserializeData(char *serializedSpace, string &serializationDirectory) {
    DESERIALIZE_VALUE(this->lane);
    DESERIALIZE_VALUE(this->tile);
    DESERIALIZE_VALUE(this->rlen);
    DESERIALIZE_VALUE(this->mate);
    DESERIALIZE_VALUE(this->readId);
    DESERIALIZE_VALUE(this->numReads);
    DESERIALIZE_VALUE(this->memorySaveMode);
    DESERIALIZE_VALUE(this->currentReadCycle);
    DESERIALIZE_VALUE(this->blockSize);
    DESERIALIZE_VALUE(this->compressedFormat);

    if (!this->memorySaveMode) {
        this->loadAlignmentsFromDisk(serializationDirectory, true);
    }
}

unsigned long SerializableAlignment::serializableDataSize() {
    unsigned long serializableSize =
        sizeof(this->lane)
            + sizeof(this->tile)
            + sizeof(this->rlen)
            + sizeof(this->mate)
            + sizeof(this->readId)
            + sizeof(this->numReads)
            + sizeof(this->memorySaveMode)
            + sizeof(this->currentReadCycle)
            + sizeof(this->blockSize)
            + sizeof(this->compressedFormat);
    return serializableSize;
}

void SerializableAlignment::freeData() {
    for (auto alignment : this->alignments) delete alignment;

    this->alignments.clear();
    // Since 'clear' doesn't reduce the actual capacity of the vector,
    // 'shrink_to_fit' is called to give back memory to the operating system.
    this->alignments.shrink_to_fit();
}

void SerializableAlignment::create_directories(string &directory) {
    std::ostringstream path_stream;
    path_stream << directory << "L00" << this->lane;

    boost::filesystem::create_directories(path_stream.str());
}

void SerializableAlignment::enableMemorySaveMode(string tempDir) {
    if (this->memorySaveMode) return;
    this->memorySaveMode = true;

    string outputFilename = get_align_fname(this->lane, this->tile, this->currentReadCycle, this->mate, tempDir);
    this->writeAlignFile(outputFilename);

    this->freeData();
}

void SerializableAlignment::disableMemorySaveMode(std::string tempDir) {
    if (!this->memorySaveMode) return;
    this->memorySaveMode = false;

    this->loadAlignmentsFromDisk(tempDir, false);
}

void SerializableAlignment::loadAlignmentsFromDisk(std::string &serializationDirectory, bool loadFromFragment) {
    std::string in_fname = get_align_fname(this->lane, this->tile, this->currentReadCycle, this->mate, serializationDirectory);
    if (loadFromFragment) in_fname += ".fragment";
    cout << "loadAlignmentsFromDisk: " + in_fname << endl;
    iAlnStream input ( blockSize, compressedFormat, nullptr );
    input.open(in_fname);

    this->alignments.reserve(this->numReads);

    for (unsigned long i = 0; i < this->numReads; i++) {
        this->alignments.push_back(input.get_alignment());
        this->alignments.back()->cycle--; // input.get_alignment() automatically increases the cycle which is not wanted in this case
    }

    input.close();

    remove(in_fname.c_str());
}

SerializableAlignment::~SerializableAlignment() {
    this->freeData();
}
