#include <utility>

#include "SerializableSequenceElements.h"

using namespace std;

SerializableSequenceElements::SerializableSequenceElements(std::vector<SequenceElement> seqs) : seqs(
        std::move(seqs)) {};

SerializableSequenceElements::SerializableSequenceElements(char *serializedSpace, string &serializationDirectory) {
    this->deserialize(serializedSpace, serializationDirectory);
};

SerializableVector<SequenceElement> SerializableSequenceElements::getSeqs() const {
    return seqs;
}

SequenceElement SerializableSequenceElements::getSeqById(uint16_t id) const {
    return seqs[id];
}

SequenceElement SerializableSequenceElements::getSeqByMate(uint16_t mate) const {
    if (mate == 0) return NULLSEQ;
    auto the_seq = seqs;
    for (uint16_t i = 0; i != the_seq.size(); i++) {
        if (the_seq[i].mate == mate) return the_seq[i];
    }
    return NULLSEQ;
}

void SerializableSequenceElements::setMateCount(uint16_t mateCount) {
    this->mateCount = mateCount;
}

uint16_t SerializableSequenceElements::getSeqCycle(uint16_t cycle, uint16_t seq_id) {
    uint16_t seq_cycle = cycle;
    for (int i = 0; i < seq_id; i++)
        seq_cycle += this->getSeqById(i).length;
    return seq_cycle;
}

uint16_t SerializableSequenceElements::getMateCycle(uint16_t mate_number, uint16_t seq_cycle) {

    // Invalid mate
    if (mate_number == 0 || mate_number > this->mateCount)
        return 0;

    // Iterate through all sequence elements (including barcodes)
    for (CountType id = 0; id < this->getSeqs().size(); id++) {

        // Current sequence element
        SequenceElement seq = this->getSeqById(id);

        // Seq is mate of interest
        if (seq.mate == mate_number)
            return (seq.length > seq_cycle ? seq_cycle : seq.length);

            // Not enough cycles left to reach mate of interest
        else if (seq.length >= seq_cycle)
            return 0;

            // Reduce number of cycles by the Seq length
        else
            seq_cycle -= seq.length;

    }

    // Should not be reached
    return 0;
}

unsigned long SerializableSequenceElements::serializableDataSize() {
    return sizeof(unsigned long) + sizeof(uint16_t) + this->seqs.serializableSize();
}

void SerializableSequenceElements::serializeData(char *serializedSpace, string &serializationDirectory) {
    (void) serializationDirectory; // UNUSED

    unsigned long size = this->serializableDataSize();
    memcpy(serializedSpace, &size, sizeof(unsigned long));
    serializedSpace += sizeof(unsigned long);

    memcpy(serializedSpace, &mateCount, sizeof(uint16_t));
    serializedSpace += sizeof(uint16_t);

    this->seqs.serialize(serializedSpace, serializationDirectory);
}

void SerializableSequenceElements::deserializeData(char *serializedSpace, string &serializationDirectory) {
    (void) serializationDirectory; // UNUSED

    unsigned long size;
    memcpy(&size, serializedSpace, sizeof(unsigned long));
    serializedSpace += sizeof(unsigned long);
    size -= sizeof(unsigned long);

    memcpy(&mateCount, serializedSpace, sizeof(uint16_t));
    serializedSpace += sizeof(uint16_t);
    size -= sizeof(uint16_t);

    // TODO: Change this with the serializableMap
    this->seqs = SerializableVector<SequenceElement>(size, serializedSpace, serializationDirectory);
}

void SerializableSequenceElements::freeData() {
    this->seqs.freeData();
}
