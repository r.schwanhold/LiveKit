#ifndef LIVEKIT_SERIALIZABLEALIGNMENT_H
#define LIVEKIT_SERIALIZABLEALIGNMENT_H

#include <string>

#include "../plugins/HiLive_sharedLibraries/global_variables.h"
#include "../plugins/HiLive_sharedLibraries/alnread.h"
#include "../plugins/HiLive_sharedLibraries/tools.h"
#include "../plugins/HiLive_sharedLibraries/tools_static.h"
#include "../plugins/HiLive_sharedLibraries/alnstream.h"

#include "../framework/FrameworkInterface.h"
#include "../framework/basecalls/data_representation/BclRepresentation.h"
#include "../framework/basecalls/data_representation/SequenceContainer.h"
#include "../framework/TileBasedFrameworkInterface.h"

class SerializableAlignment : public Serializable {
private:
    void loadAlignmentsFromDisk(std::string &serializationDirectory, bool loadFromFragment);

public:
    uint16_t lane;

    uint16_t tile;

    uint16_t rlen;

    uint16_t mate;

    uint16_t readId;

    uint32_t numReads;

    uint16_t currentReadCycle;

    bool memorySaveMode;

    std::vector<ReadAlignment *> alignments;

    uint64_t blockSize;

    uint64_t compressedFormat;

    const std::string name() const override { return "align"; };

    SerializableAlignment(
        uint16_t ln,
        uint16_t tl,
        CountType rl,
        uint16_t mate,
        uint16_t readId,
        uint32_t numReads,
        bool memorySaveMode,
        uint64_t blockSize,
        uint8_t compressedFormat
    )   : lane(ln)
        , tile(tl)
        , rlen(rl)
        , mate(mate)
        , readId(readId)
        , numReads(numReads)
        , currentReadCycle(0)
        , memorySaveMode(memorySaveMode)
        , blockSize(blockSize)
        , compressedFormat(compressedFormat) {};

    SerializableAlignment(char *serializedSpace, std::string &serializationDirectory);

    void serializeData(char *serializedSpace, std::string &serializationDirectory) override;

    void deserializeData(char *serializedSpace, std::string &serializationDirectory) override;

    unsigned long serializableDataSize() override;

    void writeAlignFile(std::string &outputFilename);

    void freeData() override;

    /**
    * Create the underlying directories of the align files.
    * @param directory where the underlying directories should be created
    */
    void create_directories(std::string &directory);

    /**
     * Sets the member variable 'memorySaveMode' to 'true' and writes align file which is needed for further computation
     * in memorySaveMode.
     */
    void enableMemorySaveMode(std::string tempDir);

    /**
     * Sets the member variable 'memorySaveMode' to 'false' and reads the align to set alignments.
     */
    void disableMemorySaveMode(std::string tempDir);


    ~SerializableAlignment() override;
};

#endif //LIVEKIT_SERIALIZABLEALIGNMENT_H
