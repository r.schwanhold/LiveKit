#ifndef LIVEKIT_BCLEXPORT_H
#define LIVEKIT_BCLEXPORT_H

#include <iostream>
#include "../../framework/FrameworkInterface.h"
#include "../../framework/fragments/Fragment.cpp"
#include "../../framework/Plugin.h"

class BclExport final : public Plugin {
public:
    std::shared_ptr<FragmentContainer> runPreprocessing(std::shared_ptr<FragmentContainer> inputFragments) override;

    std::shared_ptr<FragmentContainer> runCycle(std::shared_ptr<FragmentContainer> inputFragments) override;

    std::shared_ptr<FragmentContainer> runFullReadPostprocessing(std::shared_ptr<FragmentContainer> inputFragments) override;

    void setConfig() override;

};

#endif //LIVEKIT_BCLEXPORT_H
