#ifndef LIVEKIT_ALIGNER_H
#define LIVEKIT_ALIGNER_H

#include "../HiLive_sharedLibraries/alnread.h"
#include "../../serializables/SerializableAlignment.h"
#include "../../serializables/SerializableIndex.h"
#include "../../framework/TileBasedFrameworkInterface.h"


class Aligner {
protected:
    Configurable* settings;
    SerializableIndex *index;
    TileBasedFrameworkInterface *framework;
public:
    SerializableAlignment *alignment;
    // TODO: Document class

    Aligner(Configurable *settings, SerializableIndex *index, TileBasedFrameworkInterface *framework);

    void setAlignment(SerializableAlignment *alignment);

    /**
     * Initialize empty alignments for the current mate (stored as output of a virtual cycle 0).
     * @param mate Number of the current mate.
     */
    virtual void initAlignment() = 0;

    /**
     * Extend the alignments for all reads of the specified lane and tile by one cycle.
     * @param cycle Current cycle, i.e. the cycle that will be extended.
     * @param read_no Total number of reads.
     * @param mate Number of the current mate.
     * @param index Pointer to the reference index.
     * @return Total number of seeds (for all reads).
     */
    virtual uint64_t extendAlignment(uint16_t cycle, bool keepAlnFile) = 0;

    /**
     * Extend the barcode for all reads with the information of the current sequencing cycle.
     * @param bc_cycle The cycle of the barcode read.
     * @param read_cycle The last handled cycle for the respective mate (should always be 0 or the full length)
     * @param read_no The number of the sequence read for which the barcode will be extended (:= index in globalAlignmentSettings.seqs).
     * @param mate The read mate to extend the barcode.
     */
    virtual void extendBarcode(uint16_t bc_cycle, uint16_t currentReadLength) = 0;

    virtual ~Aligner() = default;
};


#endif //LIVEKIT_ALIGNER_H
