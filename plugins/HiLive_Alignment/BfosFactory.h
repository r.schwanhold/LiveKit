#ifndef LIVEKIT_BFOSFACTORY_H
#define LIVEKIT_BFOSFACTORY_H

#include <boost/filesystem.hpp>

#include "../../framework/configuration/Configurable.h"
#include "../../serializables/SerializableSequenceElements.h"

#include "../HiLive_sharedLibraries/alnout.h"
#include "../HiLive_sharedLibraries/tools.h"

class BfosFactory {
    Configurable *settings;
    SerializableIndex *index;
    SerializableSequenceElements *sequenceElements;

public:
    BfosFactory(Configurable *settings, SerializableIndex *index, SerializableSequenceElements *sequenceElements)
        : settings(settings)
        , index(index)
        , sequenceElements(sequenceElements) {};

    BamFileOutDeque *initBfos(uint16_t cycle);

    void finalizeBfos(BamFileOutDeque *bfos, uint16_t cycle);

    std::string getBamFileName(std::string barcode, CountType cycle);

    std::string getBamTempFileName(std::string barcode, CountType cycle);

    OutputFormat getOutputFormat();

    std::vector<std::string> get_barcode_string_vector();

    std::string get_barcode_string(CountType index) const;

    std::string format_barcode(std::string unformatted_barcode) const;

};


#endif //LIVEKIT_BFOSFACTORY_H
