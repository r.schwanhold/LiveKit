#include "MemoryAligner.h"

using namespace std;

MemoryAligner::MemoryAligner( Configurable *settings, SerializableIndex *index, TileBasedFrameworkInterface *framework)
        : Aligner(settings, index, framework) {};

void MemoryAligner::initAlignment() {
    string tempDir = this->settings->getConfigEntry<string>("tempDir");
    this->alignment->create_directories(tempDir); // TODO: Maybe do in writeAlignFile

    ReadAlignmentSettings readAlignmentSettings = ReadAlignment::generateSettingsfromConfigurable(settings);

    // write empty read alignments for each read
    for (uint32_t i = 0; i < this->alignment->numReads; ++i)
        this->alignment->alignments.push_back(new ReadAlignment(this->alignment->rlen, 0, &readAlignmentSettings));
};

uint64_t MemoryAligner::extendAlignment(uint16_t cycle, bool keepAlnFile) {
    this->alignment->currentReadCycle = cycle;

    ReadAlignmentSettings readAlignmentSettings = ReadAlignment::generateSettingsfromConfigurable(this->settings);

    const BCL &bcl = this->framework->getMostRecentBcl();

    auto numThreads = settings->getConfigEntry<uint16_t>("threadsPerTile");

    vector<thread> tile_worker;
    tile_worker.reserve(numThreads);

    vector<int> num_seeds(numThreads, 0);

    vector<char> &filterData = this->framework->getFilterData();

    // Distribute Alignment extension on multiple Threads
    for (int threadId = 0; threadId < numThreads; threadId++) {
        uint64_t startPoint = threadId * bcl.size() / numThreads;
        uint64_t endPoint   = min( ( threadId + 1 ) * bcl.size() / numThreads, bcl.size());
        tile_worker.emplace_back([&](uint64_t startPoint, uint64_t endPoint, int threadId){
            for (uint64_t i = startPoint; i < endPoint; i ++) {
                this->alignment->alignments[i]->setSettings(&readAlignmentSettings);
                if (filterData[i] == 0) {
                    this->alignment->alignments[i]->disable();
                } else {
                    this->alignment->alignments[i]->cycle = cycle;
                    this->alignment->alignments[i]->setReadGetter([=] (){
                        return this->framework->getReadIterators(i, this->alignment->readId);
                    });
                    this->alignment->alignments[i]->extend_alignment(bcl[i], index);
                    num_seeds[threadId] += this->alignment->alignments[i]->seeds.size();
                }
            }
        }, startPoint, endPoint, threadId);
    }

    for (auto &worker : tile_worker) worker.join();

    uint64_t sum_seeds = 0;
    for (auto& n : num_seeds) sum_seeds += n;


    // TODO: keepAlnFile can be computed inside the Aligner
    if ( keepAlnFile ) {
        string tempDir = this->settings->getConfigEntry<string>("tempDir");
        string outputFilename = get_align_fname(this->alignment->lane, this->alignment->tile, cycle, this->alignment->mate, tempDir);
        this->alignment->writeAlignFile(outputFilename);
    }

    return sum_seeds;
}

void MemoryAligner::extendBarcode(uint16_t bc_cycle, uint16_t currentReadLength) {
    ReadAlignmentSettings readAlignmentSettings = ReadAlignment::generateSettingsfromConfigurable(this->settings);

    BCL bcl = this->framework->getMostRecentBcl(this->alignment->lane, this->alignment->tile);
    bool keepAllBarcodes = this->settings->getConfigEntry<bool>("keepAllBarcodes");

    // Extend barcode sequence
    for (uint64_t i = 0; i < bcl.size(); i++) {
        this->alignment->alignments[i]->setSettings(&readAlignmentSettings);
        this->alignment->alignments[i]->appendNucleotideToSequenceStoreVector(bcl[i], true);

        // filter invalid barcodes if new barcode fragment is completed
        // TODO: Is done for each mate. Check if it's worth to change it (runtime should not be too high?)
        if ( !keepAllBarcodes
             && bc_cycle == currentReadLength
             && this->alignment->alignments[i]->getBarcodeIndex() == UNDETERMINED
                ) this->alignment->alignments[i]->disable();
    }
}