#ifndef LIVEKIT_EXPORTER_H
#define LIVEKIT_EXPORTER_H

#include "../../framework/configuration/Configurable.h"
#include "../../serializables/SerializableIndex.h"
#include "../../serializables/SerializableAlignment.h"
#include "../../framework/TileBasedFrameworkInterface.h"
#include "../HiLive_sharedLibraries/alnout.h"


class Exporter {
    Configurable *settings;
    SerializableIndex *index;
    TileBasedFrameworkInterface *framework;
    SerializableSequenceElements *sequenceElements;

    /** Vector containing the current cycle of all mates. */
    std::vector<CountType> mateCycles;

    uint16_t lane;
    uint16_t tile;
    uint16_t cycle;

    std::vector<SerializableAlignment *> alignmentFiles;

    /** Deque of output streams. */
    BamFileOutDeque *bfos;

    uint16_t numBarcodes;

    OutputMode outputMode;


    std::string format_barcode(std::string unformatted_barcode) const;
public:
    Exporter(
        Configurable *settings,
        SerializableIndex *index,
        TileBasedFrameworkInterface *framework,
        SerializableSequenceElements *sequenceElements,
        uint16_t lane,
        uint16_t tile,
        uint16_t cycle,
        int mateCount,
        std::vector<SerializableAlignment *> alignmentFiles,
        BamFileOutDeque *bfos
    );

    void writeTileToBam();

    bool is_mode(OutputMode alignment_mode) const;

    void setMateSAMFlags( std::vector<std::vector<seqan::BamAlignmentRecord>> & mateRecords );


};


#endif //LIVEKIT_EXPORTER_H
