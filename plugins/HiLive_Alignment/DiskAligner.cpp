#include "DiskAligner.h"

using namespace std;

DiskAligner::DiskAligner(Configurable *settings, SerializableIndex *index, TileBasedFrameworkInterface *framework)
        : Aligner(settings, index, framework) {};

void DiskAligner::initAlignment() {
    string tempDir = this->settings->getConfigEntry<string>("tempDir");
    this->alignment->create_directories(tempDir);

    // open output alignment stream
    std::string out_fname = get_align_fname(this->alignment->lane, this->alignment->tile, 0, this->alignment->mate, tempDir);
    oAlnStream output (
        this->alignment->lane,
        this->alignment->tile,
        0,
        this->alignment->rlen,
        this->alignment->numReads,
        this->alignment->blockSize,
        this->alignment->compressedFormat
    );
    output.open(out_fname);

    ReadAlignmentSettings readAlignmentSettings = ReadAlignment::generateSettingsfromConfigurable(settings);

    // write empty read alignments for each read
    for (uint32_t i = 0; i < this->alignment->numReads; ++i) {
        ReadAlignment ra(this->alignment->rlen, 0, &readAlignmentSettings);
        output.write_alignment(&ra);
    }

    if (!output.close()) std::cerr << "Error: Could not create initial alignment file." << std::endl;
}

uint64_t DiskAligner::extendAlignment(uint16_t cycle, bool keepAlnFile) {
    this->alignment->currentReadCycle = cycle; // TODO: Move into parent class?

    auto tempDir  = this->settings->getConfigEntry<string>("tempDir");
    ReadAlignmentSettings readAlignmentSettings = ReadAlignment::generateSettingsfromConfigurable(this->settings);

    const BCL &bcl = this->framework->getMostRecentBcl();
    vector<char> &filterData = this->framework->getFilterData();
    uint64_t num_reads = bcl.size(); // = input.get_num_reads();

    // 1. Open the input file
    //-----------------------
    string in_fname = get_align_fname(this->alignment->lane, this->alignment->tile, cycle - 1, this->alignment->mate, tempDir);
    iAlnStream input(this->alignment->blockSize, this->alignment->compressedFormat, &readAlignmentSettings);
    input.open(in_fname);


    // 2. Open output stream
    //----------------------------------------------------------
    string out_fname = get_align_fname(this->alignment->lane, this->alignment->tile, cycle, this->alignment->mate, tempDir);
    oAlnStream output (this->alignment->lane, this->alignment->tile, cycle, this->alignment->rlen, num_reads, this->alignment->blockSize, this->alignment->compressedFormat);
    output.open(out_fname);

    // 3. Extend alignments 1 by 1
    //-------------------------------------------------
    uint64_t num_seeds = 0;
    for (uint64_t i = 0; i < bcl.size(); i++) {
        ReadAlignment* ra = input.get_alignment();
        if (filterData[i] == 0) ra->disable();
        else {
            ra->setReadGetter([=] (){
                return this->framework->getReadIterators(i, this->alignment->readId);
            });
            ra->extend_alignment(bcl[i], index);
            num_seeds += ra->seeds.size();
        }
        output.write_alignment(ra);
        delete ra;
    }

    // 4. Close files
    //-------------------------------------------------
    if (!(input.close() && output.close())) cerr << "Could not finish alignment!" << endl;

    // 5. Delete old alignment file, if requested
    //-------------------------------------------
    if ( !keepAlnFile ) remove(in_fname.c_str());

    return num_seeds;
}

void DiskAligner::extendBarcode(uint16_t bc_cycle, uint16_t currentReadLength) {
    auto tempDir  = this->settings->getConfigEntry<string>("tempDir");

    ReadAlignmentSettings readAlignmentSettings = ReadAlignment::generateSettingsfromConfigurable(this->settings);

    // 1. Open the input file
    //-----------------------

    std::string in_fname = get_align_fname(this->alignment->lane, this->alignment->tile, this->alignment->currentReadCycle, this->alignment->mate, tempDir);
    iAlnStream input ( this->alignment->blockSize, this->alignment->compressedFormat, &readAlignmentSettings );
    input.open(in_fname);

    BCL bcl = this->framework->getMostRecentBcl(this->alignment->lane, this->alignment->tile);
    uint32_t num_reads = input.get_num_reads();
    bool keepAllBarcodes = this->settings->getConfigEntry<bool>("keepAllBarcodes");

    // 2. Open output stream
    //----------------------------------------------------------
    std::string out_fname = in_fname + ".temp";
    oAlnStream output (this->alignment->lane, this->alignment->tile, this->alignment->currentReadCycle, input.get_rlen(), num_reads, this->alignment->blockSize, this->alignment->compressedFormat);
    output.open(out_fname);

    // 3. Extend barcode sequence
    //-------------------------------------------------

    for (auto &base : bcl) {
        ReadAlignment* ra = input.get_alignment();
        ra->appendNucleotideToSequenceStoreVector(base, true);

        // filter invalid barcodes if new barcode fragment is completed
        // TODO: Is done for each mate. Check if it's worth to change it (runtime should not be too high?)
        if ( !keepAllBarcodes && bc_cycle == currentReadLength && ra->getBarcodeIndex() == UNDETERMINED )
            ra->disable();

        output.write_alignment(ra);
        delete ra;
    }

    // 4. Close files
    //-------------------------------------------------
    if (!(input.close() && output.close())) std::cerr << "Could not finish alignment!" << std::endl;

    // 5. Move temp out file to the original file.
    //-------------------------------------------
    atomic_rename(out_fname.c_str(), in_fname.c_str());
}