#include "Aligner.h"

Aligner::Aligner(Configurable *settings, SerializableIndex *index, TileBasedFrameworkInterface *framework)
    : settings(settings)
    , index(index)
    , framework(framework)
    , alignment(nullptr){}

void Aligner::setAlignment(SerializableAlignment *newAlignment) {
    this->alignment = newAlignment;
}