# Bcl2FastQ Plugin

The Bcl2FastQ plugin converts the internally used basecall representation into multiple .fastq files. <br>
With this process, the bcl2FastQ plugin uses the fragment that the ReadPositionParser outputs. <br>
This plugin does not output any Fragments.
This plugin supports different read structures (e.g. RB, RBBR, RBR). <br>
The path to the RunInfo.xml is required and have to contain the \<Reads\> tag. <br>
The SampleSheet.csv is not required but if it does not exists, see the "Parameter that have to be specified when the sampleSheet.csv does not exists" section in the [Framework config](https://gitlab.com/GeneticLife/LiveKit/wikis/configs/Framework-Config). <br>
All generated FastQ files are not gzipped yet so please make sure that you have enough free space on your disk. <br>
Also take a look at the [Bcl2FastQ config](https://gitlab.com/GeneticLife/LiveKit/wikis/configs/Bcl2FastQ-Config) values. <br>
This plugin will create a fastQ file for every sample, every lane, every read and every cycle specified in the bcl2FastQ.json.
