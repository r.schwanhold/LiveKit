from pyplugin import *

class Example(PyPlugin):
    framework = None
    permanentFragment = None

    def init(self):
        self.permanentFragment = Fragment("permanent")
        self.permanentFragment.isSerialized = False
        self.permanentFragment.set("lastLetter", 0)

    def runPreprocessing(self, inputFragments):
        print()
        print('-------------')
        print('Python Example')
        print('-------------')
        # example usage of the framework config
        num_threads = self.config["numThreads"]
        basecall_dir = self.config["BaseCallsDirectory"]
        reads = self.config["reads"]
        print('NumThreads: ', num_threads)
        print('BasecallDir: ', basecall_dir)
        print('Readstructure: ', reads)

    def runCycle(self, inputFragments):
        cycle = self.framework.getCurrentCycle()

        # execute the example code only in cycle 15 (15 is an arbitrarily number) to avoid a lot of output in each cycle
        if cycle == 15:
            # The following lines show how the methods from the PluginFrameworkInterface can be used

            # BoostPython provides a wrapper class ('vector_indexing_suite') that allows the conversion
            # from 'std::vector' (C++) to 'list' (python).
            # This wrapper class has basic operations like __len__.
            # Convert the wrapper class into a pure python list for more advanced usage (e.g. 'list(exampleWrapper)').

            # 'getSequenceRef' is not supported; use 'getSequence' instead

            print('### Start python example usage ###')

            # examples for information about the cycle
            print('CurrentCycle', self.framework.getCurrentCycle())
            print('CycleCount', self.framework.getCycleCount())
            print('CurrentReadCycle', self.framework.getCurrentReadCycle())
            print('CurrentReadId', self.framework.getCurrentReadId())
            print('CurrentReadLength', self.framework.getCurrentReadLength())
            print('isBarcode', self.framework.isBarcodeCycle())
            print('currentMateId', self.framework.getCurrentMateId())
            print('MateCount', self.framework.getMateCount())

            # examples for sequences
            seq0 = self.framework.getSequence(1, 1101, 0)
            seq1 = self.framework.getSequence(1, 1101, 1)
            print('type(getSequence): ', type(seq0))
            print('type(getSequence[0]): ', type(seq0[0]))
            print('test comparing sequences (expected True): ', seq0 == seq0)
            print('test comparing sequences (expected False): ', seq0 == seq1)
            print('getSequence size: ', len(self.framework.getSequence(1, 1101, 0)))
            print('getSequence list: ', list(self.framework.getSequence(1, 1101, 0)))
            read0 = list(self.framework.getRead(1, 1101, 0, 0)) # read with readId 0 of sequence 0
            print('content of read0 before change: ', read0)
            read0.clear()  # Attention: this only changes the content of the local copy
            print('content of read0 after change: ', read0)
            print('content of read1: ', list(self.framework.getRead(1, 1101, 0, 1))) # because we are in cycle 15 this should be empty

            # examples for bcls
            most_recent_bcl = self.framework.getMostRecentBcl(1, 1101)
            print('getMostRecentBcl size: ', len(most_recent_bcl))
            print('getMostRecentBcl list: ', list(most_recent_bcl))
            most_recent_bcls = self.framework.getMostRecentBcls(1)
            print('getMostRecentBcls size: ', len(most_recent_bcls))
            print('mostRecentBcls[0] == getMostRecentBcl (expected is "True"):   ', list(most_recent_bcls[0]) == list(most_recent_bcl))

            # examples for filterData
            filter_data = self.framework.getFilterData(1, 1101)
            print('filterBasecall size: ', len(filter_data))
            print('filterBasecall list: ', list(filter_data))
            print('filterBasecall: ', self.framework.filterBasecall(1, 1101, 0))

            # example for numSequences
            print('getNumSequences', self.framework.getNumSequences(1, 1101))

            print('### Finished python example usage ###')
