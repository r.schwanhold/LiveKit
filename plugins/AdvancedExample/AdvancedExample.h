#ifndef LIVEKIT_ADVANCEDEXAMPLE_H
#define LIVEKIT_ADVANCEDEXAMPLE_H

#include <iostream>
#include "../../framework/Plugin.h"
#include "../../framework/FrameworkInterface.h"
#include "../../framework/fragments/Fragment.cpp"

class AdvancedExample final : public Plugin {
public:

    LIVEKIT_TILE_CONTEXT(AdvancedExampleTileContext) {
    public:
        // This method analyzes the ALIGN-Fragments from the Hilive-Plugin
        std::shared_ptr<FragmentContainer> runCycleForTile(std::shared_ptr<FragmentContainer> inputFragments) override;
    };
};

#endif //LIVEKIT_ADVANCEDEXAMPLE_H
