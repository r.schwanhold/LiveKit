#include "AdvancedExample.h"
#include "../../serializables/SerializableAlignment.h"

using namespace std;

std::shared_ptr<FragmentContainer> AdvancedExample::AdvancedExampleTileContext::runCycleForTile(std::shared_ptr<FragmentContainer> inputFragments) {

    int currentMateId = this->framework->getCurrentMateId();
    int currentCycle = this->framework->getCurrentCycle();

    auto alignment = (SerializableAlignment *) inputFragments->get("ALIGN")->getSerializable("mate_" + to_string(currentMateId));

    uint16_t alignmentLane = alignment->lane;
    uint16_t alignmentTile = alignment->tile;
    uint16_t alignmentRlen = alignment->rlen;
    uint16_t alignmentMate = alignment->mate;
    uint16_t alignmentReadId = alignment->readId;
    uint32_t alignmentNumReads = alignment->numReads;
    uint16_t alignmentCurrentReadCycle = alignment->currentReadCycle;
    std::vector<ReadAlignment *> readAlignments = alignment->alignments;
    uint64_t blockSize = alignment->blockSize;
    uint64_t compressedFormat = alignment->compressedFormat;

    cout << "###  Cycle " + to_string(currentCycle) + "  ###" << endl;
    cout << "alignmentLane " + to_string(alignmentLane) << endl;
    cout << "alignmentTile " + to_string(alignmentTile) << endl;
    cout << "alignmentRlen " + to_string(alignmentRlen) << endl;
    cout << "alignmentMate " + to_string(alignmentMate) << endl;
    cout << "alignmentReadId " + to_string(alignmentReadId) << endl;
    cout << "alignmentNumReads " + to_string(alignmentNumReads) << endl;
    cout << "alignmentCurrentReadCycle " + to_string(alignmentCurrentReadCycle) << endl;
    cout << "readAlignments size " + to_string(readAlignments.size()) << endl;
    cout << "blockSize " + to_string(blockSize) << endl;
    cout << "compressedFormat " + to_string(compressedFormat) << endl;
    cout << endl;

    auto outputContainer = this->framework->createNewFragmentContainer();
    return outputContainer;
}

extern "C" AdvancedExample *create() {
    return new AdvancedExample;
}

extern "C" void destroy(AdvancedExample *plugin) {
    delete plugin;
}
