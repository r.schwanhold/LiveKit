#ifndef LIVEKIT_EXAMPLE_H
#define LIVEKIT_EXAMPLE_H

#include <iostream>
#include "../../framework/Plugin.h"
#include "../../framework/FrameworkInterface.h"
#include "../../framework/fragments/Fragment.cpp"

class Example final : public Plugin {
public:

    void init() override;

    std::shared_ptr<FragmentContainer> runCycle(std::shared_ptr<FragmentContainer> inputFragments) override;

    void finalize() override {};

    void setConfig() override;
};

#endif //LIVEKIT_EXAMPLE_H
