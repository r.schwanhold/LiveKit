#ifndef ALNOUT_H
#define ALNOUT_H

#include "headers.h"
#include "definitions.h"
#include "global_variables.h"
#include "kindex.h"
#include "tools.h"
#include "alnstream.h"
#include "alnread.h"
#include "parallel.h"
#include "../../framework/configuration/Configurable.h"
#include "../../framework/basecalls/management/BaseManager.h"

/**
 * Extension of SeqAn's BamFileOut data type supporting multithreading via an atomic flag.
 * @author Tobias Loka
 */
struct Atomic_bfo {

private:

    /** The BamFileOut stream.*/
    seqan::BamFileOut bfo;

    /** The atomic flag to perform a spinlock while writing.*/
    std::atomic_flag flag = ATOMIC_FLAG_INIT;

    /**
     * Lock the atomic flag.
     */
    void lock() {
        while (flag.test_and_set(std::memory_order_acquire)); // spin
    }

    /**
     * Unlock the atomic flag.
     */
    void unlock() {
        flag.clear(std::memory_order_release);
    }

public:

    /**
     * Default constructor that also initializes the BamFileOut stream.
     * @param f_name Output file name.
     * */
    Atomic_bfo(std::string f_name) : bfo(f_name.c_str()) {}

    /** Destructor. */
    ~Atomic_bfo() {}

    /**
     * Set the context of the BamFileOut stream.
     * @param context The new context.
     */
    void setContext(seqan::BamIOContext<seqan::StringSet<seqan::CharString> > &context) {
        bfo.context = context;
    }

    /**
     * Write the header to the output file.
     * @param header The header for the output file.
     */
    void writeHeader(seqan::BamHeader &header) {
        seqan::writeHeader(bfo, header);
    }

    /**
     * Write records to the output file in a "thread-safe" manner.
     * @param records Reference to a vector containing a set of records.
     */
    void writeRecords(std::vector<seqan::BamAlignmentRecord> &records) {

        if (records.size() == 0)
            return;

        lock();

        seqan::writeRecords(bfo, records);

        unlock();

    }

    /**
     * Write records to the output file in a "thread-safe" manner.
     * @param records Reference to a vector containing an other vector of records.
     */
    void writeRecords(std::vector<std::vector<seqan::BamAlignmentRecord>> &records) {

        if (records.size() == 0)
            return;

        lock();

        for (auto &subvector : records)
            seqan::writeRecords(bfo, subvector);

        unlock();

    }

};

/**
 * Extends a deque of atomic BamFileOut streams.
 * Store the context, refNames and refNamesCache such that it exist only once and will not be destructed as long as it is needed.
 * @author Tobias Loka
 */
class BamFileOutDeque {

    /** Deque of bfos. */
    std::deque<Atomic_bfo> bfos;

    /** All fields needed for the context of the BamFileOut streams. */
    seqan::BamIOContext<seqan::StringSet<seqan::CharString> > context;
    seqan::StringSet<seqan::CharString> refNames;
    seqan::NameStoreCache<seqan::StringSet<seqan::CharString> > refNamesCache;

public:

    /**
     * Set the context of the list.
     * @param seq_names Names of all sequences in the database
     * @param seq_length Lengths of all sequences in the database
     */
    void set_context(std::vector<std::string> &seq_names, std::vector<uint32_t> &seq_lengths) {
        seqan::NameStoreCache<seqan::StringSet<seqan::CharString> > rnc(refNames);
        refNamesCache = rnc;
        seqan::BamIOContext<seqan::StringSet<seqan::CharString> > cxt(refNames, refNamesCache);
        context = cxt;
        seqan::contigNames(context) = seq_names;
        seqan::contigLengths(context) = seq_lengths;
    }

    /**
     * Add a Bam Output stream for the given file name.
     * The stream will be created in-place.
     * @param f_name File name for the Bam output stream.
     */
    void emplace_back(std::string f_name) {
        bfos.emplace_back(f_name.c_str());
        bfos.back().setContext(context);
    }

    /**
     * Get a reference to the last atomic bfo of the deque.
     * @return Reference to the last atomic bfo of the deque.
     */
    Atomic_bfo &back() { return bfos.back(); }

    /**
     * Get the Atomic_bfo at a certain position of the deque.
     * @param i Index of the Atomic_bfo in the deque.
     * @return reference to the Atomic_bfo at a certain position of the deque.
     */
    Atomic_bfo &operator[](int i) { return bfos[i]; }

    /**
     * Get the size of the underlying deque.
     * @returm Size of the underlying deque.
     */
    size_t size() {
        return bfos.size();
    }

    /**
     * Clear the deque of Atomic bfos.
     */
    void clear() {
        bfos.clear();
    }
};

#endif /* ALNSTREAM_H */
