#include <iostream>
#include <vector>
#include "../framework/FrameworkInterface.h"
#include "../framework/Framework.h"
#include "./catch2/catch.hpp"


TEST_CASE("FrameworkInterfaceTests") {
    // TODO: comment this in once the deadlock is fixed
    // The methods of the CycleManager are already tested in 'CycleManagerTests' and will therefor not be featured here.
    /*
    std::string manifestFilePath = "../tests/mockFiles/minimalFramework.json";
    auto *framework = new Framework(manifestFilePath);
    framework->run();

    FrameworkInterface frameworkInterface(framework);
    frameworkInterface.setCurrentPluginCycle(8); // 8 is the last cycle (reads: 2R 2B 2B 2R)

    SECTION("Should be able to get the currentPluginCycle"){
        REQUIRE(frameworkInterface.getCurrentCycle() == 8);
    }

    SECTION("Should be able to get the reference of a sequence"){

        // 'getSequenceRef' returns a reference to the "full" sequence that has been parsed, no matter in which cycle the plugin is
        Sequence &seq1 = frameworkInterface.getSequenceRef(1, 1101, 0);

        REQUIRE(seq1[0].size() == 2);
        REQUIRE(seq1[1].size() == 2);
        REQUIRE(seq1[2].size() == 2);
        REQUIRE(seq1[3].size() == 2);
        REQUIRE(seq1[0] == Read{0x9C, 0xA0}); // these are each the first base from cycle 1 and 2

        frameworkInterface.setCurrentPluginCycle(5);
        Sequence &seq2 = frameworkInterface.getSequenceRef(1, 1101, 0);

        REQUIRE(seq1 == seq2);

        seq1[0] = Read{0x11, 0x22};

        REQUIRE(seq1[0] == Read{0x11, 0x22});
        REQUIRE(seq2[0] == Read{0x11, 0x22});
    }

    SECTION("Should be able to get the correct sequence depending on the 'currentPluginCycle'"){

        Sequence seq1 = frameworkInterface.getSequence(1, 1101, 0);

        REQUIRE(seq1[0].size() == 2);
        REQUIRE(seq1[1].size() == 2);
        REQUIRE(seq1[2].size() == 2);
        REQUIRE(seq1[3].size() == 2);
        REQUIRE(seq1[0] == Read{0x9C, 0xA0}); // these are each the first base from cycle 1 and 2

        frameworkInterface.setCurrentPluginCycle(5);
        Sequence seq2 = frameworkInterface.getSequence(1, 1101, 0);

        REQUIRE_FALSE(seq1 == seq2);

        REQUIRE(seq2[0].size() == 2);
        REQUIRE(seq2[1].size() == 2);
        REQUIRE(seq2[2].size() == 1); // <-- this is the important information
        REQUIRE(seq2[3].size() == 0); // <-- this is the important information
        REQUIRE(seq2[0] == Read{0x9C, 0xA0}); // these are each the first base from cycle 1 and 2
    }

    SECTION("Should return the correct concatenated sequence"){

        Read read1 = frameworkInterface.getConcatenatedSequence(1, 1101, 0);

        REQUIRE(read1 == Read{0x9C, 0xA0, 0x9C, 0x9E, 0xA3, 0x9E, 0x9F, 0x9F}); // these are each the first base from cycle 1 to 8

        frameworkInterface.setCurrentPluginCycle(5);
        Read read2 = frameworkInterface.getConcatenatedSequence(1, 1101, 0);

        REQUIRE_FALSE(read1 == read2);

        REQUIRE(read2 == Read{0x9C, 0xA0, 0x9C, 0x9E, 0xA3}); // these are each the first base from cycle 1 to 5
    }

    SECTION("Should return the correct BCLs for a specific tile"){
        std::deque<BCL> bcls1 = frameworkInterface.getMostRecentBcls(1);
        REQUIRE(bcls1.size() == 1); // we only have one tile (1101)
        REQUIRE(bcls1[0].size() == frameworkInterface.getNumSequences(1, 1101));
    }*/
}