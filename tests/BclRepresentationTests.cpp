#include <iostream>
#include <vector>
#include "../framework/basecalls/data_representation/BclRepresentation.h"
#include "../framework/basecalls/parser/BclParser.h"
#include "./catch2/catch.hpp"

TEST_CASE("BclRepresentationTests") {

    std::vector<uint16_t> lanes = {1};
    std::vector<uint16_t> tiles = {1101};
    BclRepresentation bclRepresentation(lanes, tiles); // 1 lane, 1 tiles
    uint16_t lane = 1; // first lane
    uint16_t tile = 1101; // first tile

    SECTION("Should be able to add BCLs") {
        for (int i = 1; i <= 3; i++) { // this simulates multiple cycles
            auto bcl = BCL(1, (char) 64 + i);
            bclRepresentation.addBcl(1, 1101, i, bcl); // adds an BCL with only one char (some ascii characters: A, B, C)
        }

        REQUIRE(bclRepresentation.getBcl(lane, tile, 3) == BCL(1, 'C')); // last added
        REQUIRE(bclRepresentation.getBcl(lane, tile, 2) == BCL(1, 'B')); // second last added
        REQUIRE(bclRepresentation.getBcl(lane, tile, 1) == BCL(1, 'A')); // first added

        REQUIRE(bclRepresentation.getBcl(1, 1101, 3) == BCL(1, 'C')); // last added
        REQUIRE(bclRepresentation.getBcl(1, 1101, 2) == BCL(1, 'B')); // second last added
        REQUIRE(bclRepresentation.getBcl(1, 1101, 1) == BCL(1, 'A')); // first added
    }

    SECTION("Should be able to serialize the BCL") {
        auto bcl = BCL(1, (char) 65);
        bclRepresentation.addBcl(1, 1101, 1, bcl);

        REQUIRE(!bclRepresentation.isSerialized(1, 1101, 1));
        bclRepresentation.serialize(1, 1101, 1);
        REQUIRE(bclRepresentation.isSerialized(1, 1101, 1));
    }
}

